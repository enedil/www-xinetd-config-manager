(set-config-directory "/tmp/test/ttt")
(set-config-file-name "/tmp/test/config")
(set-user "enedil")
(add-challenge 
  (make-challenge 
    :name "zadanko1" 
    :server "/tmp/test/zadanka/zadanie1.py" 
    :server_args NIL
    :port 8888
    :flag "CRYPTO{omg}"))
(add-challenge 
  (make-challenge 
    :name "zadanko2" 
    :server "/usr/bin/env"
    :server_args (list "python3" "/tmp/test/zadanka/zadanie2.py")
    :port 8889
    :flag "CRYPTO{wuut}"))
